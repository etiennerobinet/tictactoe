/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.db;

import game.dto.GameDTO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import game.specification.GameSpecification;

/**
 *
 * @author Reda
 */
public class GameDbTest {

    public GameDbTest() {
    }

    /**
     * Test of getCollection method, of class GameDb.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetCollection() throws Exception {
        DBManager.getConnection();
        DBManager.startTransaction();
        System.out.println("getCollection Game");
        
        GameDb instance = GameDb.getInstance();
        int current = SequenceDB.getInstance().getNextNum("GAMES")-1;
        int next = current +1;
        Date date = null;
        SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd");
        Date dateE = dateF.parse("2019-12-23");
        GameDTO item = new GameDTO(next, dateE, "X", "X");
        
        GameSpecification sel = new GameSpecification(current);
        instance.getCollection(sel);
        
        List<GameDTO> items = new ArrayList<>();
        items = GameDb.getAllGames();
        
        List<GameDTO> result = new ArrayList<>();
        result.add(item);
        assertEquals(items, result);
    }

    /**
     * Test of deleteDb method, of class GameDb.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testDeleteDb() throws Exception {
        DBManager.getConnection();
        DBManager.startTransaction();
        System.out.println("delete Game");
        GameDb instance = GameDb.getInstance();
        int current = SequenceDB.getInstance().getNextNum("GAMES")-1;
        
        instance.deleteDb(current);
        
        List<GameDTO> items = new ArrayList<>();
        items = GameDb.getAllGames();
        
        List<GameDTO> result = new ArrayList<>();
        assertEquals(items, result);
        
    }

    /**
     * Test of updateDb method, of class GameDb.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testUpdateDb() throws Exception {
        DBManager.getConnection();
        DBManager.startTransaction();
        System.out.println("update Game");
        GameDb instance = GameDb.getInstance();
        int current = SequenceDB.getInstance().getNextNum("GAMES")-1;
        int next = current +1;
        Date date = new Date();
        SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd");
        Date dateE = dateF.parse(dateF.format(date));
        GameDTO item = new GameDTO(next, dateE, "X", "O");
        
        instance.updateDb(item);
        
        List<GameDTO> items = new ArrayList<>();
        items = GameDb.getAllGames();
        
        List<GameDTO> result = new ArrayList<>();
        assertEquals(items, result);
    }

    /**
     * Test of insertDb method, of class GameDb.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testInsertDb() throws Exception {
        DBManager.getConnection();
        DBManager.startTransaction();
        System.out.println("insert Game");
        GameDb instance = GameDb.getInstance();
        int current = SequenceDB.getInstance().getNextNum("GAMES");
        int next = current + 1;

        Date date = new Date();
        SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd");
        Date dateE = dateF.parse(dateF.format(date));
        GameDTO item = new GameDTO(next, dateE, "X", "X");

        List<GameDTO> items = new ArrayList<>();
        int num = instance.insertDb(item);
        assertEquals(next, num);
        current = SequenceDB.getNextNum("GAMES");
        assertEquals(next, num);
        
        GameSpecification sel = new GameSpecification(current);
        List<GameDTO> result = new ArrayList<>();
        result = GameDb.getAllGames();
        items.add(result.get(0));
        items.add(item);
        
        assertEquals(items, result);
        DBManager.cancelTransaction();
        
        
    }

}
