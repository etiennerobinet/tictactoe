/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.db;

import game.dto.SquareDTO;
import game.specification.SquareSpecification;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Reda
 */
public class SquaresDbTest {

    public SquaresDbTest() {
    }

    /**
     * Test of getCollection method, of class SquaresDb.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetCollection() throws Exception {
        DBManager.getConnection();
        DBManager.startTransaction();
        System.out.println("getCollection Square");

        SquaresDb instance = SquaresDb.getInstance();
        int current = SequenceDB.getInstance().getNextNum("SQUARES") - 1;
        int next = current + 1;

        SquareDTO item = new SquareDTO(1, 1, 1, "X");
        SquareDTO item2 = new SquareDTO(2, 1, 2, "O");

        SquareSpecification sel = new SquareSpecification(next);
        instance.getCollection(sel);

        List<SquareDTO> items = new ArrayList<>();
        items = SquaresDb.getAllSquares();

        List<SquareDTO> result = new ArrayList<>();
        result.add(item);
        result.add(item2);
        assertEquals(items, result);
    }

    /**
     * Test of updateDb method, of class SquaresDb.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testUpdateDb() throws Exception {
        DBManager.getConnection();
        DBManager.startTransaction();
        System.out.println("update Squares");
        SquaresDb instance = SquaresDb.getInstance();
        int current = SequenceDB.getNextNum("SQUARES") - 1;

        SquareDTO item = new SquareDTO(1, 1, 1, "X");
        SquareDTO item2 = new SquareDTO(2, 1, 3, "O");

        SquaresDb.updateDb(item2);

        List<SquareDTO> items = new ArrayList<>();
        items = SquaresDb.getAllSquares();

        List<SquareDTO> result = new ArrayList<>();
        result.add(item);
        result.add(item2);
        assertEquals(items, result);
    }

    /**
     * Test of deleteDb method, of class SquaresDb.
     */
    @Test
    public void testDeleteDb() throws Exception {
        DBManager.getConnection();
        DBManager.startTransaction();
        System.out.println("delete Square");
        SquaresDb instance = SquaresDb.getInstance();
        int current = SequenceDB.getInstance().getNextNum("SQUARES") - 1;

        System.out.println(current);
        instance.deleteDb(current+1);

        List<SquareDTO> items = new ArrayList<>();
        items = SquaresDb.getAllSquares();

        SquareDTO item = new SquareDTO(1, 1, 1, "X");
        List<SquareDTO> result = new ArrayList<>();
        result.add(item);
        assertEquals(items, result);
    }

    /**
     * Test of insertDb method, of class SquaresDb.
     */
    @Test
    public void testInsertDb() throws Exception {
        DBManager.getConnection();
        DBManager.startTransaction();
        System.out.println("insert Square");
        SquaresDb instance = SquaresDb.getInstance();
        int current = SequenceDB.getInstance().getNextNum("SQUARES");
        int next = current ;

        SquareDTO item = new SquareDTO(next, 1, 5, "X");

        List<SquareDTO> items = new ArrayList<>();
        int num = instance.insertDb(item);
        assertEquals(next, num);
        current = SequenceDB.getNextNum("SQUARES");
        assertEquals(next, num);
        
        SquareSpecification sel = new SquareSpecification(current);
        List<SquareDTO> result = new ArrayList<>();
        result = SquaresDb.getAllSquares();
        items.add(result.get(0));
        items.add(result.get(1));
        items.add(item);
        
        //assertEquals(items, result);
        DBManager.cancelTransaction();
    }

}
