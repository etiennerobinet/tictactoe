/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.dto;

import game.exception.DtoException;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author G43283
 */
public class GameDTO extends EntityDto<Integer>{
    
    private int gameId;
    private LocalDate endDate;
    private String opponent;
    private String opponentWon;

    public GameDTO(int id, LocalDate endDate, String opponent, String opponentWon) {
        this.id = id;
        this.endDate = endDate;
        this.opponent = opponent;
        this.opponentWon = opponentWon;
    }

    public GameDTO(LocalDate now, String opponent, String opponentWon) throws DtoException {
        if (now == null || opponent == null || opponentWon == null) {
            throw new DtoException("les attributs date, opponent et opponentWon sont obligatoires");
        }
        this.endDate = now;
        this.opponent = opponent;
        this.opponentWon = opponentWon;
    }

    /**
     *
     * @return
     */
    public int getGameId() {
       return this.gameId;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public String getOpponent() {
        return opponent;
    }

    public String getOpponentWon() {
        return opponentWon;
    }
    
    public void setId(int id) {
        this.gameId = id;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }

    public void setOpponentWon(String opponentWon) {
        this.opponentWon = opponentWon;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GameDTO other = (GameDTO) obj;
       if (!Objects.equals(this.gameId, other.gameId)) {
            return false;
        }
        if (!Objects.equals(this.opponent, other.opponent)) {
            return false;
        }
        if (!Objects.equals(this.opponentWon, other.opponentWon)) {
            return false;
        }
        if (!Objects.equals(this.endDate, other.endDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GameDTO{" + "id=" + this.gameId + ", endDate=" + endDate + ", opponent=" + opponent + ", opponentWon=" + opponentWon + '}';
    }
    
}
