/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.dto;

import game.exception.DtoException;
import game.model.Move;
import java.util.Objects;

/**
 *
 * @author G43283
 */
public class SquareDTO extends EntityDto<Integer>{
    private int squareId;
    private int idGames;
    private int position;
    private Move move;

    public SquareDTO(int squareId, int idGames, int position, Move move) {
        this.squareId = squareId;
        this.idGames = idGames;
        this.position = position;
        this.move = move;
    }

    public SquareDTO(int idGames, int position, Move move) {
        this.idGames = idGames;
        this.position = position;
        this.move = move;
    }

    
    
    public int getSquareId() {
        return this.squareId;
    }

    public int getIdGames() {
        return idGames;
    }

    public int getPosition() {
        return position;
    }

    public Move getMove() {
        return move;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdGames(int idGames) {
        this.idGames = idGames;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setMove(Move move) {
        this.move = move;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SquareDTO other = (SquareDTO) obj;
        if (this.squareId != other.squareId) {
            return false;
        }
        if (this.idGames != other.idGames) {
            return false;
        }
        if (this.position != other.position) {
            return false;
        }
        if (!Objects.equals(this.move, other.move)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SquareDTO{" + "id=" + squareId + ", idGames=" + idGames + ", position=" + position + ", move=" + move + '}';
    }
    
    
}
