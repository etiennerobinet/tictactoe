/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.specification;

/**
 *
 * @author Reda
 */
public class SquareSpecification {
    
    private int id;
    private int idGames;
    private int position;
    private String item;

    public SquareSpecification(int id, int idGames, int position, String item) {
        this.id = id;
        this.idGames = idGames;
        this.position = position;
        this.item = item;
    }

    
    public SquareSpecification(int position, String item) {
        this.id = 0;
        this.idGames = 0;
        this.position = position;
        this.item = item;
    }

    public SquareSpecification(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getIdGames() {
        return idGames;
    }

    public int getPosition() {
        return position;
    }

    public String getItem() {
        return item;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdGames(int idGames) {
        this.idGames = idGames;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setItem(String item) {
        this.item = item;
    }
    
}
