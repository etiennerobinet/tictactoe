package game.specification;

import java.time.LocalDate;

/**
 *
 * @author jlc
 */
public class GameSpecification {

    private int id;
    private LocalDate endDate;
    private String opponent;
    private String opponentWon;

    
    public GameSpecification(String opponent, String opponentWon) {
        this.id = 0;
        this.endDate = LocalDate.now();
        this.opponent = opponent;
        this.opponentWon = opponentWon;
    }
    
    public GameSpecification (String winner){
        this.opponentWon=winner;
        this.id=0;
    }

    /**
     *
     * @param id
     */
    public GameSpecification(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    public String getOpponent() {
        return opponent;
    }

    public String getOpponentWon() {
        return opponentWon;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }

    public void setOpponentWon(String opponentWon) {
        this.opponentWon = opponentWon;
    }

}
