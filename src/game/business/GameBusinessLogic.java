package game.business;

import game.db.GameDb;
import game.dto.GameDTO;
import game.exception.BusinessException;
import game.exception.DbException;
import game.model.Move;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import game.specification.GameSpecification;

class GameBusinessLogic {

    static int add(GameDTO user) throws DbException, ParseException {
        return GameDb.insertDb(user);
    }

    static void delete(int id) throws DbException {
        GameDb.deleteDb(id);
    }

    static int getWins(Move move) throws DbException, ParseException {
        var games = GameDb.getCollection(new GameSpecification("FALSE"));
        return games.size();
    }

    static void update(GameDTO game) throws DbException {
        GameDb.updateDb(game);
    }

    static GameDTO findById(int id) throws DbException, ParseException {
        GameSpecification sel = new GameSpecification(id);
        Collection<GameDTO> col = GameDb.getCollection(sel);
        if (col.size() == 1) {
            return col.iterator().next();
        } else {
            return null;
        }
    }

    static List<GameDTO> findBySel(GameSpecification sel) throws DbException, ParseException {
        List<GameDTO> col = GameDb.getCollection(sel);
        return col;
    }

    static List<GameDTO> findAll() throws DbException, ParseException {
        GameSpecification sel = new GameSpecification(0);
        List<GameDTO> col = GameDb.getCollection(sel);
        return col;
    }
   

}
