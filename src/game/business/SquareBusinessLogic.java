package game.business;

import game.exception.BusinessException;
import game.exception.DbException;
import game.specification.SquareSpecification;
import game.dto.SquareDTO;
import game.db.SquaresDb;
import java.util.Collection;
import java.util.List;


class SquareBusinessLogic {

    /**
     * Insert an user in the database. Returns the user's id.
     *
     * @param login user's login.
     * @param name user's name.
     * @return the user's id.
     * @throws SQLException if the query failed.
     */
    static int add(SquareDTO square) throws DbException {
        return SquaresDb.insertDb(square);
    }

    /**
     * Removes the given id.
     *
     * @param id id to delete.
     * @throws SQLException if the query failed.
     */
    static void delete(int id) throws DbException {
        SquaresDb.deleteDb(id);
    }

    /**
     * Updates the given game.
     *
     * @param game game to update.
     * @throws SQLException if the query failed.
     */
    static void update(SquareDTO game) throws DbException {
        SquaresDb.updateDb(game);
    }


    /**
     * Returns the unique user with the given id.
     *
     * @param id user's id.
     * @return the unique user with the given id.
     * @throws SQLException if the query failed.
     */
    static SquareDTO findById(int id) throws DbException{
        SquareSpecification sel = new SquareSpecification(id);
        Collection<SquareDTO> col = SquaresDb.getCollection(sel);
        if (col.size() == 1) {
            return col.iterator().next();
        } else {
            return null;
        }
    }

    /**
     * Returns a list of users whith the gibven specifications.
     *
     * @param sel specifications (where clause)
     * @return a list of users whith the gibven specifications.
     * @throws BusinessException if the query failed.
     */
    static List<SquareDTO> findBySel(SquareSpecification sel) throws DbException{
        List<SquareDTO> col = SquaresDb.getCollection(sel);
        return col;
    }

    /**
     * Returns a list of all users.
     *
     * @return a list of all users.
     * @throws BusinessException if the query failed.
     */
    static List<SquareDTO> findAll() throws DbException{
        SquareSpecification sel = new SquareSpecification(0);
        List<SquareDTO> col = SquaresDb.getCollection(sel);
        return col;
    }

    //ajouter les methodes avec une logique métier
}
