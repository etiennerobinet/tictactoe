package game.business;

import game.dto.GameDTO;
import game.dto.SquareDTO;
import game.exception.BusinessException;
import game.exception.DbException;
import game.model.Move;
import java.time.LocalDate;
import java.util.List;

/**
 * Administrator facade.
 */
public interface AdminFacade {

    List<GameDTO> getGame() throws BusinessException;

    GameDTO getGame(int id) throws BusinessException;

    int addGame(LocalDate date, String opponent, String opponentWon) throws BusinessException;

    void removeGame(GameDTO game) throws BusinessException;
  
    void updateGame(GameDTO current) throws BusinessException;

    List<SquareDTO> getSquares() throws BusinessException;

    SquareDTO getSquare(int id) throws BusinessException;

    public int addSquare(int gameId, int pos, Move move) throws DbException, BusinessException;

    void removeSquare(SquareDTO game) throws BusinessException;
  
    void updateSquare(SquareDTO current) throws BusinessException;
}
