package game.business;

import game.db.DBManager;
import game.db.GameDb;
import game.dto.GameDTO;
import game.dto.SquareDTO;
import game.exception.BusinessException;
import game.exception.DbException;
import game.exception.DtoException;
import game.model.Move;
import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Run the program fr the administrator.
 */
public class AdminModel implements AdminFacade {

    private static final String URL = "jdbc:sqlite:ressources/sqliteDB/tictactoe.db";
    private static final String USER_DB = "app";
    private static final String PASSWORD = "app";

    static final String USERS = "USERS";

    static int getNextNum(String sequence) throws SQLException {
        Connection connexion = DriverManager.getConnection(URL, USER_DB, PASSWORD);
        String query = "Update SEQUENCES set sValue = sValue+1 where id='" + sequence + "'";
        java.sql.PreparedStatement update = connexion.prepareStatement(query);
        update.execute();
        java.sql.Statement stmt = connexion.createStatement();
        query = "Select sValue FROM Sequences where id='" + sequence + "'";
        java.sql.ResultSet rs = stmt.executeQuery(query);
        int nvId;
        if (rs.next()) {
            nvId = rs.getInt("sValue");
            return nvId;
        } else {
            throw new IllegalStateException("Nouveau n° de séquence inaccessible!");
        }
    }

    @Override
    public List<GameDTO> getGame() throws BusinessException {
        try {
            DBManager.startTransaction();
            List<GameDTO> col = GameBusinessLogic.findAll();
            DBManager.validateTransaction();
            return col;
        } catch (DbException eDB) {
            String msg = eDB.getMessage();
            try {
                DBManager.cancelTransaction();
            } catch (DbException ex) {
                msg = ex.getMessage() + "\n" + msg;
            } finally {
                throw new BusinessException("Liste des Game inaccessible! \n" + msg);
            }
        } catch (ParseException ex) {
            Logger.getLogger(AdminModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int addGame(LocalDate date, String opponent, String opponentWon) throws BusinessException {
        try {
            DBManager.startTransaction();
            GameDTO game = new GameDTO(date, opponent, opponentWon);
            int id = GameBusinessLogic.add(game);
            DBManager.validateTransaction();
            return id;
        } catch (DbException | DtoException | ParseException ex) {
            Logger.getLogger(AdminModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    @Override
    public GameDTO getGame(int gameId) throws BusinessException {
        try {
            DBManager.startTransaction();
            GameDTO user = GameBusinessLogic.findById(gameId);
            DBManager.validateTransaction();
            return user;
        } catch (DbException eDB) {
            String msg = eDB.getMessage();
            try {
                DBManager.cancelTransaction();
            } catch (DbException ex) {
                msg = ex.getMessage() + "\n" + msg;
            } finally {
                throw new BusinessException("Liste des Games inaccessible! \n" + msg);
            }
        } catch (ParseException ex) {
            Logger.getLogger(AdminModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public void removeGame(GameDTO game) throws BusinessException {
        try {
            if (game.isPersistant()) {
                DBManager.startTransaction();
                GameBusinessLogic.delete(game.getId());
                DBManager.validateTransaction();
            } else {
                throw new BusinessException("User: impossible de supprimer un user inexistant!");
            }
        } catch (DbException eDB) {
            String msg = eDB.getMessage();
            try {
                DBManager.cancelTransaction();
            } catch (DbException ex) {
                msg = ex.getMessage() + "\n" + msg;
            } finally {
                throw new BusinessException("Suppression de User impossible! \n" + msg);
            }
        }
    }

    @Override
    public void updateGame(GameDTO game) throws BusinessException {
        try {
            DBManager.startTransaction();
            GameBusinessLogic.update(game);
            DBManager.validateTransaction();
        } catch (DbException eDB) {
            String msg = eDB.getMessage();
            try {
                DBManager.cancelTransaction();
            } catch (DbException ex) {
                msg = ex.getMessage() + "\n" + msg;
            } finally {
                throw new BusinessException("Mise à jour de User impossible! \n" + msg);
            }
        }
    }

    @Override
    public List<SquareDTO> getSquares() throws BusinessException {
        try {
            DBManager.startTransaction();
            List<SquareDTO> col = SquareBusinessLogic.findAll();
            DBManager.validateTransaction();
            return col;
        } catch (DbException eDB) {
            String msg = eDB.getMessage();
            try {
                DBManager.cancelTransaction();
            } catch (DbException ex) {
                msg = ex.getMessage() + "\n" + msg;
            } finally {
                throw new BusinessException("Liste des Squares inaccessible! \n" + msg);
            }
        }
    }

    @Override
    public SquareDTO getSquare(int id) throws BusinessException {
        try {
            DBManager.startTransaction();
            SquareDTO user = SquareBusinessLogic.findById(id);
            DBManager.validateTransaction();
            return user;
        } catch (DbException eDB) {
            String msg = eDB.getMessage();
            try {
                DBManager.cancelTransaction();
            } catch (DbException ex) {
                msg = ex.getMessage() + "\n" + msg;
            } finally {
                throw new BusinessException("Liste des Square inaccessible! \n" + msg);
            }
        }
    }

    @Override
    public int addSquare(int gameId, int pos, Move move) throws DbException, BusinessException {
        DBManager.startTransaction();
        SquareDTO square = new SquareDTO(gameId, pos, move);
        int sucess = SquareBusinessLogic.add(square);
        DBManager.validateTransaction();
        return sucess;
    }

    @Override
    public void removeSquare(SquareDTO square) throws BusinessException {
        try {
            if (square.isPersistant()) {
                DBManager.startTransaction();
                SquareBusinessLogic.delete(square.getId());
                DBManager.validateTransaction();
            } else {
                throw new BusinessException("User: impossible de supprimer un user inexistant!");
            }
        } catch (DbException eDB) {
            String msg = eDB.getMessage();
            try {
                DBManager.cancelTransaction();
            } catch (DbException ex) {
                msg = ex.getMessage() + "\n" + msg;
            } finally {
                throw new BusinessException("Suppression de Square impossible! \n" + msg);
            }
        }
    }

    @Override
    public void updateSquare(SquareDTO square) throws BusinessException {
        try {
            DBManager.startTransaction();
            SquareBusinessLogic.update(square);
            DBManager.validateTransaction();
        } catch (DbException eDB) {
            String msg = eDB.getMessage();
            try {
                DBManager.cancelTransaction();
            } catch (DbException ex) {
                msg = ex.getMessage() + "\n" + msg;
            } finally {
                throw new BusinessException("Mise à jour de Square impossible! \n" + msg);
            }
        }
    }

    public int getWIns(Move move) throws DbException, ParseException {
        return GameBusinessLogic.getWins(move);
    }

    public void clearDB() throws DbException, SQLException {
        DBManager.startTransaction();
        java.sql.Connection connexion = DBManager.getConnection();
        java.sql.Statement stmt = connexion.createStatement();
        String query = "DROP TABLE GAMES;"
                + "DROP TABLE SQUARES;"
                + "DROP TABLE SEQUENCES;";
        stmt.execute(query);
        query = "CREATE TABLE GAMES (\n"
                + "	id 			BIGINT 			NOT NULL,\n"
                + "	endDate 	DATE			NOT NULL,\n"
                + "	opponent 	CHAR			NOT NULL,\n"
                + "	opponentWon CHAR			NOT NULL,\n"
                + "	PRIMARY KEY (id)\n"
                + "	);\n"
                + "	\n"
                + "CREATE TABLE SQUARES (\n"
                + "	id			BIGINT			NOT NULL,\n"
                + "    idGames		BIGINT			NOT NULL,\n"
                + "	position 	BIGINT			NOT NULL,\n"
                + "	item		CHAR			NOT NULL,\n"
                + "	PRIMARY KEY (id)\n"
                + "	FOREIGN KEY(idGames) REFERENCES GAMES(id)\n"
                + "	);\n"
                + "	\n"
                + "CREATE TABLE SEQUENCES (\n"
                + "        id      VARCHAR(50)             NOT NULL,\n"
                + "        sValue  numeric(10)             NOT NULL,\n"
                + "        constraint IDSEQUENCE primary key (id)\n"
                + "        );";
        stmt.execute(query);

        DBManager.validateTransaction();
        DBManager.startTransaction();
        query = "INSERT INTO SEQUENCES VALUES ('SQUARES', 1);\n"
                + "INSERT INTO SEQUENCES VALUES ('GAMES', 1);";
        stmt.execute(query);
        DBManager.cancelTransaction();

    }
}
