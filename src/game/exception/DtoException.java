/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.exception;

/**
 *
 * @author G43283
 */
public class DtoException extends Exception {

    /**
     * Creates a new instance of <code>DtoException</code> without detail message.
     */
    public DtoException() {
    }


    /**
     * Constructs an instance of <code>DtoException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public DtoException(String msg) {
        super(msg);
    }
}