/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.view;

import game.db.DBManager;
import game.exception.DbException;
import game.model.Move;
import game.model.Observer;
import game.model.SaveThread;
import game.model.Status;
import game.model.TicTacToe;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * FXML Controller class
 *
 * @author Etienne Robinet <43823@he2b.be>
 */
public class ViewController implements Observer, Initializable {

    @FXML
    Button newGameButton;
    @FXML
    Label opponentLabel;
    @FXML
    Label statusLabel;
    @FXML
    Label gamesWon;
    @FXML
    ComboBox<Move> playerComboBox;

    @FXML
    GridPane grid;

    TicTacToe game;

    public ViewController() {
    }

    public void setGame(TicTacToe tictactoe) {
        game = tictactoe;
    }

    @FXML
    void newGame() {
        if (this.game != null) {
            if (game.matchIsOver() || game.getStatus() == Status.EMPTY) {
                this.game.initialize();
                Move move = playerComboBox.getSelectionModel().getSelectedItem();
                this.game.startMatch(move);
            }
            else if(game.getStatus()==Status.IN_COURSE){
                showAlert("Game Running");
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        playerComboBox.getItems().add(Move.X);
        playerComboBox.getItems().add(Move.O);
        playerComboBox.getSelectionModel().selectFirst();
        if (grid != null) {
            initGrid();
        }
    }

    @Override
    public void update(Object arg) {
        if (arg!=null && arg instanceof String){
            showAlert((String) arg);
        }
        else if (game.matchIsOver()) {
            save();
            updateBoard();
            showRestart();
            statusLabel.setText(game.getStatus().toString());
        } else if (game.getStatus() != Status.EMPTY) {
            try {
                opponentLabel.setText(game.getOpponentMove().toString());
                statusLabel.setText(game.getStatus().toString());
                gamesWon.setText(String.valueOf(game.getWins(game.getPlayerMove())));
                playerComboBox.getSelectionModel().select(game.getPlayerMove());
                playerComboBox.setEditable(false);
                updateBoard();
            } catch (DbException | ParseException ex) {
                showAlert(ex.getMessage());
            }
        }
    }

    private void showAlert(String message) {
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle(message);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    private void showRestart() {
        if (game.matchIsOver()) {
            Alert dialog = new Alert(AlertType.CONFIRMATION);
            dialog.setContentText("Do you want to start a new game?");
            dialog.setTitle("Game is over");
            dialog.setHeaderText(null);
            ButtonType yesBttn = new ButtonType("Yes", ButtonData.YES);
            ButtonType noBttn = new ButtonType("No", ButtonData.NO);
            dialog.getButtonTypes().clear();
            dialog.getButtonTypes().addAll(yesBttn, noBttn);
            Optional<ButtonType> result = dialog.showAndWait();
            if (result.get() == yesBttn) {
                newGame();
            } else if (result.get() == noBttn) {
                System.exit(0);
            }
        }
    }

    void initGrid() {
        grid.setStyle("-fx-background-color:#dae7f3;");
        grid.setAlignment(Pos.CENTER);
        for (Node node : grid.getChildren()) {
            node.setOnMouseEntered((MouseEvent t) -> {
                node.setStyle("-fx-background-color:#4E9DF7;");
            });
            node.setOnMouseExited((MouseEvent t) -> {
                node.setStyle("-fx-background-color:#dae7f3;");
            });
            node.setOnMouseClicked((MouseEvent e) -> {
                if (!game.matchIsOver() && game.getStatus() != Status.EMPTY) {
                    int row;
                    int col;
                    if (GridPane.getColumnIndex(node) == null) {
                        col = 0;
                    } else {
                        col = GridPane.getColumnIndex(node);
                    }
                    if (GridPane.getRowIndex(node) == null) {
                        row = 0;
                    } else {
                        row = GridPane.getRowIndex(node);
                    }
                    int target = 3 * row + (col);
                    try {
                        game.play(target);
                        game.opponentPlay();
                    } catch (IllegalArgumentException ex) {
                        showAlert(ex.getMessage());
                    }
                }
            });

        }

    }

    private void updateBoard() {
        grid.setAlignment(Pos.CENTER);
        for (int i = 0; i < 9; i++) {
            int col = i / 3;
            int row = i % 3;
            Pane cell = (Pane) getNodeFromGridPane(grid, row, col);
            GridPane.setHalignment(cell, HPos.CENTER);
            GridPane.setValignment(cell, VPos.CENTER);
            if (game.getBoard().get(i) == null) {
                if (cell.getChildren().isEmpty()) {
                    Label blank = new Label();
                    blank.setFont(new Font("Arial", 100));
                    GridPane.setHalignment(blank, HPos.CENTER);
                    GridPane.setValignment(blank, VPos.CENTER);
                    cell.getChildren().add(new Label(""));
                } else {
                    for (Node node : cell.getChildren()) {
                        if (node instanceof Label) {
                            ((Label) node).setText("");
                            ((Label) node).setFont(new Font("Consolas", 100));
                        }
                    }
                }
            } else {
                switch (game.getBoard().get(i)) {
                    case X:
                        if (cell.getChildren().isEmpty()) {
                            Label xLbl = new Label("X");
                            xLbl.setFont(new Font("Consolas", 100));
                            GridPane.setHalignment(xLbl, HPos.CENTER);
                            GridPane.setValignment(xLbl, VPos.CENTER);
                            cell.getChildren().add(xLbl);
                        } else {
                            for (Node node : cell.getChildren()) {
                                if (node instanceof Label) {
                                    ((Label) node).setText("X");
                                    ((Label) node).setFont(new Font("Consolas", 100));
                                    ((Label) node).setTextFill(Color.web("#CC0000"));
                                }
                            }
                        }
                        break;
                    case O:
                        if (cell.getChildren().isEmpty()) {
                            Label oLbl = new Label("O");
                            oLbl.setFont(new Font("Consolas", 100));
                            GridPane.setHalignment(oLbl, HPos.CENTER);
                            GridPane.setValignment(oLbl, VPos.CENTER);
                            cell.getChildren().add(oLbl);
                        } else {
                            for (Node node : cell.getChildren()) {
                                if (node instanceof Label) {
                                    ((Label) node).setText("O");
                                    ((Label) node).setFont(new Font("Consolas", 100));
                                    ((Label) node).setTextFill(Color.web("#27BC3A"));
                                }
                            }
                        }
                        break;
                    default:

                        break;
                }
            }
        }
    }

    private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
        Integer x;
        Integer y;
        if (col == 0) {
            y = null;
        } else {
            y = col;
        }
        if (row == 0) {
            x = null;
        } else {
            x = row;
        }
        for (Node node : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == y && GridPane.getRowIndex(node) == x) {
                return node;
            }
        }
        return null;
    }

    private void save() {
        SaveThread save = new SaveThread(game.getDb(),game);
        save.addObserver(this);
        Platform.runLater(save);
    }
    
    @FXML
    private void reset() throws DbException{
        try {
            game.getDb().clearDB();
            if(game.getStatus()!=Status.EMPTY){
                game.initialize();
                newGame();
            }
        } catch (DbException | SQLException ex) {
           DBManager.cancelTransaction();
           showAlert(ex.getMessage());
        }
        
    }
}
