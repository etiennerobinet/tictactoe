package game.model;

import game.exception.ModelException;

/**
 * Facade of the TicTacToe game.
 *
 * TicTacToe is a game for two players, X and O, who take turns marking the
 * spaces in a 3×3 grid. The player who succeeds in placing three of their marks
 * in a horizontal, vertical, or diagonal row is the winner.
 *
 * @see
 * <a href="https://en.wikipedia.org/wiki/Facade"> Link to a description of the
 * design pattern facade  </a>
 *
 *
 * @author JLC Contact jlechien@he2b.be
 *
 *
 */
public interface Facade {

    /**
     * Initializes the board, the player and the status of the game.
     *
     */
    void initialize();

    /**
     * Sets the players move (X or O) and checks if all the components are ready
     * (board, player).
     *
     * @param playerMove the move of the human player.
     */
    void startMatch(Move playerMove);

    /**
     * Return true if the game is over and false otherwise.
     *
     * @return true if the game is over and false otherwise.
     */
    boolean matchIsOver();

    /**
     * Plays the human's move on the board. The number given is used as a
     * coordinate. The first row of the board as the numbers 0, 1 and 2. The
     * second row of the board as the numbers 3, 4 and 5. The third row of the
     * board as the numbers 6, 7 and 8. Any other number throws an
     * IllegalArgumentException.
     *
     * After the move, all data are saved if the game is over and a notification
     * is send to all observers.
     *
     * @param number coordinate of the board, the number has to be between 0 and
     * 8.
     */
    void play(int number);

    /**
     * Set the human player to X and the opponent to O.
     */
    void setMoveToX();

    /**
     * Set the human player to O and the opponent to X.
     */
    void setMoveToO();

    /**
     * The opponent checks the board and play his move. If the game is over, all
     * data are saved.
     */
    void opponentPlay();

    /**
     * Returns a copy of the board.
     *
     * @return a copy of the board.
     */
    Board getBoard();

    /**
     * Returns the status of the game : EMPTY, IN_COURSE, WON, LOST, DRAW.
     *
     * @return the status of the game.
     */
    Status getStatus();

    /**
     * Returns the human's move (X or O).
     *
     * @return the human's move (X or O).
     */
    Move getPlayerMove();

    /**
     * Returns the opponent's move (X or O).
     *
     * @return the opponent's move (X or O).
     */
    Move getOpponentMove();

    /**
     * Adds an observer to the list of observers for this object)
     *
     * @param observer an observer to be added.
     */
    void addObserver(Observer observer);

    /**
     * Notify all of its observers.
     */
    void notifyObsevers();

    /**
     * Deletes an observer from the list of observers of this object. Passing
     * null to this method will have no effect.
     *
     * @param observer the observer to be deleted.
     */
    void removeObserver(Observer observer);

    /**
     * Quits the program and close all data access if necessary.
     */
    void quit();
}
