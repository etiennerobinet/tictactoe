package game.model;

import static game.model.Board.SIZE;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author jlc
 */
class Opponent extends Player{

    private List<Integer> avalaibles;

    Opponent() {
        this.avalaibles = new ArrayList<>();
    }

    int selectPosition() {
        if (avalaibles.isEmpty()) {
            throw new IllegalStateException("Aucune position disponible");
        }
        Collections.shuffle(avalaibles);
        return avalaibles.get(0);
    }

    void readBoard(Board board) {
        Objects.requireNonNull(board, "Aucun plateau");
        avalaibles.clear();
        for (int position = 0; position < SIZE * SIZE; position++) {
            if (board.isFree(position)) {
                avalaibles.add(position);
            }
        }
    }
}
