/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.model;

import game.business.AdminModel;
import game.db.SequenceDB;
import game.exception.BusinessException;
import game.exception.DbException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Etienne Robinet <43823@he2b.be>
 */
public class SaveThread extends Thread {

    private AdminModel db;
    private TicTacToe game;

    private List<Observer> observers;

    public SaveThread(AdminModel db, TicTacToe game) {
        this.db = db;
        this.game = game;
        observers = new ArrayList<>();
    }

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void notifyObsevers() {
        for (Observer observer : observers) {
            observer.update(this);
        }
    }

    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    public void run() {
        if (game.matchIsOver()) {
            try {
                String opponent = game.getOpponentMove().toString();
                String opponentWon = game.getStatus()==Status.WON?"False":"True";
                int id=db.addGame(LocalDate.now(), opponent, opponentWon);
                game.getBoard().save(db, id);
                notifyObservers("Saved!");
                return;
            } catch (DbException | BusinessException ex) {
                Logger.getLogger(SaveThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void notifyObservers(Object message) {
        for (Observer obs : observers) {
            obs.update(message);
        }
    }

}
