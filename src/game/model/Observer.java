package game.model;

/**
 *
 * @author jlc
 */
public interface Observer {

    /**
     *
     * @param arg
     */
    void update(Object arg);
}
