package game.model;

/**
 *
 * @author jlc
 */
public enum Status {

    /**
     *
     */
    EMPTY,

    /**
     *
     */
    IN_COURSE,

    /**
     *
     */
    WON,

    /**
     *
     */
    LOST,

    /**
     *
     */
    DRAW;
}
