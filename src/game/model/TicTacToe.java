package game.model;

import game.business.AdminModel;
import game.exception.DbException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author jlc
 */
public class TicTacToe implements Facade {

    private Board board;
    private Opponent opponent;
    private Player player;
    private Status status;
    private AdminModel db;

    private List<Observer> observers;

    /**
     *
     */
    public TicTacToe(AdminModel db) throws DbException {
        board = new Board();
        player = new Player();
        opponent = new Opponent();
        status = Status.EMPTY;
        observers = new ArrayList<>();
        this.db = db;
    }

    public AdminModel getDb() {
        return db;
    }
    

    @Override
    public void initialize() {
        board.initialize();
        player.initialize();
        opponent.initialize();
        status = Status.EMPTY;
    }

    @Override
    public void startMatch(Move playerMove) {
        Objects.requireNonNull(playerMove, "le joueur doit sélectionner sa lettre");
        status = Status.IN_COURSE;
        player.setMove(playerMove);
        if (playerMove == Move.O) {
            opponent.setMove(Move.X);
        } else {
            opponent.setMove(Move.O);
        }

        if (!board.isEmpty()) {
            throw new IllegalStateException("Le plateau n'est pas vide");
        }
        if (!player.isReady()) {
            throw new IllegalStateException("Le joueur n'a pas séléectionné sa pièce");
        }
        if (!opponent.isReady()) {
            throw new IllegalStateException("L'adversaire n'a pas séléectionné sa pièce");
        }
        notifyObsevers();
    }

    @Override
    public boolean matchIsOver() {
        return status == Status.DRAW
                || status == Status.LOST
                || status == Status.WON;
    }

    /**
     *
     * @param number
     */
    @Override
    public void play(int number) {
        board.put(player.getMove(), number);
        if (board.check(player.getMove())) {
            status = Status.WON;
        }
        if (status != Status.WON && board.isFull()) {
            status = Status.DRAW;
        }
        notifyObsevers();
    }

    /**
     *
     */
    @Override
    public void setMoveToX() {
        player.setMove(Move.X);
        opponent.setMove(Move.O);
        notifyObsevers();
    }

    /**
     *
     */
    @Override
    public void setMoveToO() {
        player.setMove(Move.O);
        opponent.setMove(Move.X);
        notifyObsevers();
    }

    /**
     *
     */
    @Override
    public void opponentPlay() {
            opponent.readBoard(board);
            int opponentMove = opponent.selectPosition();
            board.put(opponent.getMove(), opponentMove);
            if (board.check(opponent.getMove())) {
                status = Status.LOST;
            }
            if (status != Status.LOST && board.isFull()) {
                status = Status.DRAW;
            }
            notifyObsevers();
    }

    public int getWins(Move move) throws DbException, ParseException{
        return db.getWIns(move);
    }
    public void save() throws InterruptedException{
     SaveThread save = new SaveThread(db,this);
     save.start();
     save.join();
    }

    /**
     *
     * @param observer
     */
    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    /**
     *
     */
    @Override
    public void notifyObsevers() {
        for (Observer observer : observers) {
            observer.update(this);
        }
    }

    /**
     *
     * @param observer
     */
    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    /**
     *
     * @return
     */
    @Override
    public Board getBoard() {
        return new Board(board);
    }

    /**
     *
     * @return
     */
    @Override
    public Status getStatus() {
        return status;
    }

    /**
     *
     * @return
     */
    @Override
    public Move getPlayerMove() {
        return player.getMove();
    }

    /**
     *
     * @return
     */
    @Override
    public Move getOpponentMove() {
        return opponent.getMove();
    }


    /**
     *
     */
    @Override
    public void quit() {
    }
    
    
    

}

