package game.model;

import game.business.AdminModel;
import game.db.SequenceDB;
import game.dto.SquareDTO;
import game.exception.BusinessException;
import game.exception.DbException;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author jlc
 */
public class Board {

    static final int SIZE = 3;
    private Move[][] moves;

    Board() throws DbException, DbException, DbException {
        moves = new Move[SIZE][SIZE];
    }

    Board(Board other) {
        Objects.requireNonNull(other, "Aucun plateau");
        moves = new Move[SIZE][SIZE];
        for (int row = 0; row < SIZE; row++) {
            for (int column = 0; column < SIZE; column++) {
                moves[row][column] = other.moves[row][column];
            }
        }
    }

    void initialize() {
        for (int row = 0; row < SIZE; row++) {
            for (int column = 0; column < SIZE; column++) {
                moves[row][column] = null;
            }
        }
    }

    boolean isEmpty() {
        boolean isEmpty = true;
        int row = 0;
        while (isEmpty && row < SIZE) {
            int column = 0;
            while (isEmpty && column < SIZE) {
                isEmpty = moves[row][column] == null;
                column++;
            }
            row++;
        }
        return isEmpty;
    }

    boolean isFull() {
        boolean isFull = true;
        int row = 0;
        while (isFull && row < SIZE) {
            int column = 0;
            while (isFull && column < SIZE) {
                isFull = moves[row][column] != null;
                column++;
            }
            row++;
        }
        return isFull;
    }

    boolean isFree(int number) {
        requireNumber(number);
        int row = number / SIZE;
        int column = number % SIZE;
        return moves[row][column] == null;
    }

    void put(Move move, int number) {
        requireNumber(number);
        int row = number / SIZE;
        int column = number % SIZE;
        if (moves[row][column] != null) {
            throw new IllegalArgumentException("La case " + number + " est déjà remplie");
        }
        moves[row][column] = move;
    }

    boolean check(Move move) {
        Objects.requireNonNull(move, "Aucun mouvement en paramètre");
        return checkRows(move)
                || checkColumns(move)
                || checkDiagonal(move)
                || checkAntiDiagonal(move);
    }

    /**
     *
     * @param number
     * @return
     */
    public Move get(int number) {
        requireNumber(number);
        int row = number / SIZE;
        int column = number % SIZE;
        return moves[row][column];
    }

    private void requireNumber(int number) {
        if (number < 0 || number >= SIZE * SIZE) {
            throw new IllegalArgumentException("La position doit "
                    + "être comprise entre 0 et " + SIZE * SIZE);
        }
    }

    private boolean checkRow(int row, Move move) {
        int column = 0;
        while (column < moves[row].length && moves[row][column] == move) {
            column++;
        }
        return column == SIZE;
    }

    private boolean checkColumn(int column, Move move) {
        int row = 0;
        while (row < moves.length && moves[row][column] == move) {
            row++;
        }
        return row == SIZE;
    }

    private boolean checkDiagonal(Move move) {
        int row = 0;
        int column = 0;
        while (row < moves.length && moves[row][column] == move) {
            row++;
            column++;
        }
        return row == SIZE;
    }

    private boolean checkAntiDiagonal(Move move) {
        int row = SIZE - 1;
        int column = 0;
        while (column < moves.length && moves[row][column] == move) {
            row--;
            column++;
        }
        return column == SIZE;
    }

    private boolean checkRows(Move move) {
        boolean isValid = false;
        int row = 0;
        while (!isValid && row < SIZE) {
            isValid = checkRow(row, move);
            row++;
        }
        return isValid;
    }

    private boolean checkColumns(Move move) {
        boolean isValid = false;
        int column = 0;
        while (!isValid && column < SIZE) {
            isValid = checkColumn(column, move);
            column++;
        }
        return isValid;
    }

    public synchronized void save(AdminModel db, int gameId) throws DbException, BusinessException {
        for (int pos = 0; pos < SIZE * SIZE; pos++) {
            if (!isFree(pos)) {
                Move move = get(pos);
                db.addSquare(gameId, pos, move);
            }
        }
    }
}
