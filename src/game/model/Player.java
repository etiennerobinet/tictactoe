package game.model;

/**
 *
 * @author jlc
 */
public class Player {

    /**
     *
     */
    protected Move move;

    Player() {
    }

    void initialize() {
        move = null;
    }

    /**
     *
     * @return
     */
    public Move getMove() {
        return move;
    }

    boolean isReady() {
        return move != null;
    }

    void setMove(Move move) {
        this.move = move;
    }

}
