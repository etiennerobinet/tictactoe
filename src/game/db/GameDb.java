/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.db;

import game.dto.GameDTO;
import game.exception.DbException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import game.specification.GameSpecification;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author G43283
 */
public class GameDb {

    private static final String recordName = "GAMES";
    
    private static GameDb instance;
    
    public static GameDb getInstance(){
        if(instance == null){
            instance = new GameDb();
        }
        return instance;
    }
    
    public static List<GameDTO> getAllGames() throws DbException, ParseException {
        List<GameDTO> games = getCollection(new GameSpecification(0));
        return games;
    }

   
    /**
     *
     * @param sel
     * @return
     * @throws DbException
     */
    public static List<GameDTO> getCollection(GameSpecification sel) throws DbException, ParseException {
        List<GameDTO> al = new ArrayList<>();
        try {
            String query = "Select id, endDate, opponent, opponentWon  FROM GAMES ";
            java.sql.Connection connexion = DBManager.getConnection();
            java.sql.PreparedStatement stmt;
            String where = "";
            if (sel.getId() != 0) {
                where = where + " id = ? ";
            }
            if (sel.getEndDate() != null && !sel.getEndDate().toString().isEmpty()) {
                if (!where.isEmpty()) {
                    where = where + " AND ";
                }
                where = where + " endDate like ? ";
            }

            if (sel.getOpponent() != null && !sel.getOpponent().isEmpty()) {
                if (!where.isEmpty()) {
                    where = where + " AND ";
                }
                where = where + " opponent like ? ";
            }

            if (sel.getOpponentWon() != null && !sel.getOpponentWon().isEmpty()) {
                if (!where.isEmpty()) {
                    where = where + " AND ";
                }
                where = where + " opponentWon like ? ";
            }

            if (where.length() != 0) {
                where = " where " + where + " order by endDate, opponent, opponentWon";
                query = query + where;
                stmt = connexion.prepareStatement(query);
                int i = 1;
                if (sel.getId() != 0) {
                    stmt.setInt(i, sel.getId());
                    i++;
                }
                if (sel.getEndDate() != null && !sel.getEndDate().toString().isEmpty()) {
                    stmt.setString(i, sel.getEndDate() + "%");
                    i++;
                }
                if (sel.getOpponent() != null && !sel.getOpponent().isEmpty()) {
                    stmt.setString(i, sel.getOpponent() + "%");
                    i++;
                }
                if (sel.getOpponentWon() != null && !sel.getOpponentWon().isEmpty()) {
                    stmt.setString(i, sel.getOpponentWon() + "%");
                    i++;
                }
            } else {
                query = query + " Order by endDate, opponent, opponentWon";
                stmt = connexion.prepareStatement(query);
            }
            java.sql.ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String date = rs.getString("endDate");
                SimpleDateFormat dateF = new SimpleDateFormat("dd-MM-YYYY");
                Date d = dateF.parse(date);
                LocalDate ld =d.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                al.add(new GameDTO(id, ld,
                       rs.getString("opponent"), rs.getString("opponentWon")));
            }
        } catch (java.sql.SQLException eSQL) {
            throw new DbException("Instanciation de " + recordName + " impossible:\nSQLException: " + eSQL.getMessage());
        }
        return al;
    }

    /**
     *
     * @param id
     * @throws DbException
     */
    public static void deleteDb(int id) throws DbException {
        try {
            java.sql.Statement stmt = DBManager.getConnection().createStatement();
            stmt.execute("delete from Games where id=" + id);
            stmt.execute("delete from Squares where idGames=" +id);
        } catch (DbException | SQLException ex) {
            throw new DbException(recordName + ": suppression impossible\n" + ex.getMessage());
        }
    }

    /**
     *
     * @param record
     * @throws DbException
     */
    public static void updateDb(GameDTO record) throws DbException {
        try {
            java.sql.Connection connexion = DBManager.getConnection();

            java.sql.PreparedStatement update;
            String sql = "Update GAMES set "
                    + "id=?, "                    
                    + "endDate=?, "
                    + "opponent=?, "
                    + "opponentWon=? "
                    + "where id=?";
            update = connexion.prepareStatement(sql);
            update.setInt(1, record.getGameId());    
           LocalDate d = record.getEndDate();
            update.setObject(2, d);
            update.setString(3, record.getOpponent());
            update.setString(4, record.getOpponentWon());
            update.setInt(5, record.getGameId());
            update.executeUpdate();
            
        } catch (DbException | SQLException ex) {
            throw new DbException(recordName+", modification impossible:\n" + ex.getMessage());
        }
    }

    /**
     *
     * @param record
     * @return
     * @throws DbException
     */
    public static int insertDb(GameDTO record) throws DbException {
        try {
            int num = SequenceDB.getNextNum(SequenceDB.GAMES);
            java.sql.Connection connexion = DBManager.getConnection();
            java.sql.PreparedStatement insert;
            insert = connexion.prepareStatement(
                    "Insert into GAMES(id, endDate, opponent, opponentWon) "
                    + "values(?, ?, ? , ?)");
            insert.setInt(1, num);
            LocalDate d = record.getEndDate();
            insert.setObject(2, d);
            insert.setString(3, record.getOpponent());
            insert.setString(4, record.getOpponentWon());
            insert.executeUpdate();
            return num;
        } catch (DbException | SQLException ex) {
            throw new DbException(recordName + ": ajout impossible\n" + ex.getMessage());
        }
    }

   
}
