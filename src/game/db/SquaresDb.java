/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.db;

import game.dto.GameDTO;
import game.dto.SquareDTO;
import game.exception.DbException;
import game.model.Move;
import game.specification.GameSpecification;
import game.specification.SquareSpecification;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Reda
 */
public class SquaresDb {

    private static final String recordName = "SQUARES";

    private static SquaresDb instance;

    public static SquaresDb getInstance() {
        if (instance == null) {
            instance = new SquaresDb();
        }
        return instance;
    }

    public static List<SquareDTO> getAllSquares() throws DbException, ParseException {
        List<SquareDTO> squares = getCollection(new SquareSpecification(0));
        return squares;
    }

    public static GameDTO getGame(SquareDTO record) throws DbException, ParseException{
        List<GameDTO> col = GameDb.getCollection(new GameSpecification(record.getIdGames()));
        if(col.size()==1){
            return col.get(0);
        }
        return null;
    }
    public static List<SquareDTO> getCollection(SquareSpecification sel) throws DbException {
        List<SquareDTO> al = new ArrayList<>();
        try {
            String query = "Select id, idGames, position, item  FROM SQUARES";
            java.sql.Connection connexion = DBManager.getConnection();
            java.sql.PreparedStatement stmt;
            String where = "";
            if (sel.getId() != 0) {
                where = where + " id = ? ";
            }
            if (sel.getIdGames() != 0) {
                if (!where.isEmpty()) {
                    where = where + " AND ";
                }
                where = where + " idGames like ? ";
            }

            if (sel.getPosition() != 0) {
                if (!where.isEmpty()) {
                    where = where + " AND ";
                }
                where = where + " position like ? ";
            }

            if (sel.getItem() != null && !sel.getItem().isEmpty()) {
                if (!where.isEmpty()) {
                    where = where + " AND ";
                }
                where = where + " item like ? ";
            }

            if (where.length() != 0) {
                where = " where " + where + " order by idGames, position, item";
                query = query + where;
                stmt = connexion.prepareStatement(query);
                int i = 1;
                if (sel.getId() != 0) {
                    stmt.setInt(i, sel.getId());
                    i++;
                }
                if (sel.getIdGames() != 0) {
                    stmt.setString(i, sel.getIdGames() + "%");
                    i++;
                }
                if (sel.getPosition() != 0) {
                    stmt.setString(i, sel.getPosition() + "%");
                    i++;
                }
                if (sel.getItem() != null && !sel.getItem().isEmpty()) {
                    stmt.setString(i, sel.getItem() + "%");
                    i++;
                }
            } else {
                query = query + " Order by id, idGames, position, item";
                stmt = connexion.prepareStatement(query);
            }
            java.sql.ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");

                al.add(new SquareDTO(id, rs.getInt("idGames"),
                        rs.getInt("position"), Move.valueOf(rs.getString("item"))));
            }
        } catch (java.sql.SQLException eSQL) {
            throw new DbException("Instanciation de " + recordName + " impossible:\nSQLException: " + eSQL.getMessage());
        }
        return al;
    }

    public static void deleteDb(int id) throws DbException {
        try {
            java.sql.Statement stmt = DBManager.getConnection().createStatement();
            stmt.execute("delete from Squares where id=" + id);
        } catch (DbException | SQLException ex) {
            throw new DbException(recordName + ": suppression impossible\n" + ex.getMessage());
        }
    }

    public static void updateDb(SquareDTO record) throws DbException {
        try {
            java.sql.Connection connexion = DBManager.getConnection();

            java.sql.PreparedStatement update;
            String sql = "Update SQUARES set "
                    + "id=?, "
                    + "idGames=?, "
                    + "position=?, "
                    + "item=? "
                    + "where id=?";
            update = connexion.prepareStatement(sql);
            update.setInt(1, record.getSquareId());
            update.setInt(2, record.getIdGames());
            update.setInt(3, record.getPosition());
            update.setString(4, record.getMove().toString());
            update.setInt(5, record.getSquareId());
            update.executeUpdate();

        } catch (DbException | SQLException ex) {
            throw new DbException(recordName + ", modification impossible:\n" + ex.getMessage());
        }
    }

    public static int insertDb(SquareDTO record) throws DbException {
        try {
            int num = SequenceDB.getNextNum(SequenceDB.SQUARES);
            java.sql.Connection connexion = DBManager.getConnection();
            java.sql.PreparedStatement insert;
            insert = connexion.prepareStatement(
                    "Insert into SQUARES(id, idGames, position, item) "
                    + "values(?, ?, ? , ?)");
            insert.setInt(1, num);
            
            insert.setInt(2, record.getIdGames());
            insert.setInt(3, record.getPosition());
            insert.setString(4, record.getMove().toString());
            insert.executeUpdate();
            return num;
        } catch (DbException | SQLException ex) {
            throw new DbException(recordName + ": ajout impossible\n" + ex.getMessage());
        }
    }
}
