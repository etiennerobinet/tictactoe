
package game;

import game.business.AdminFacade;
import game.business.AdminModel;
import game.model.TicTacToe;
import game.view.ViewController;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{
    
    private Scene scene ;
    private AdminModel facade;
    private TicTacToe game;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }



    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/view.fxml"));
        scene = new Scene(loader.load());
        ViewController view = loader.getController();
        facade = new AdminModel();
        game = new TicTacToe(facade);
        view.setGame(game);
        game.addObserver(view);
        
        
        stage.setResizable(false);
        stage.setTitle("TicTacToe");
        stage.setScene(scene);
        stage.show();
    }

}
